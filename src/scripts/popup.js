import { routes as routePath, status as scrappingStatus } from '../constants';

const heading = document.getElementById('title');
const routes = document.getElementById('routes');
const back = document.getElementById('to-home');
const scrapper = document.getElementById('at-scrapper');
const copier = document.getElementById('at-copier');
const feedScrapper = document.getElementById('at-feed-scrapper');
const crScrapper = document.getElementById('at-cr-scrapper');
const errorContainer = document.getElementById('error');

const setHTML = (route) => {
  chrome.storage.local.get([route], (values) => {
    if (values && values[route]) {
      const { status, data } = values[route];
      const page = document.querySelector(`[data-route=${route}`);

      if (status) {
        const statusElement = page.getElementsByClassName('status')[0];

        if (statusElement) {
          statusElement.innerHTML = status;
        }
      }

      if (data) {
        const dataElement = page.getElementsByClassName('data')[0];

        if (dataElement) {
          dataElement.innerHTML = data;
        }
      }
    }
  });
}

const setError = (errorMessage) => {
  errorContainer.innerHTML = errorMessage;

  setTimeout(() => { errorContainer.innerHTML = '' ;}, 3000);
}

document.getElementById('to-scrapper').onclick = () => {
  heading.innerHTML = "Inbox Scrapper";
  routes.style.display = 'none';
  back.style.display = 'block';
  scrapper.style.display = 'block';

  setHTML(routePath.inboxScrapper);
}

document.getElementById('to-copier').onclick = () => {
  heading.innerHTML = "Cookie Copier";
  routes.style.display = 'none';
  back.style.display = 'block';
  copier.style.display = 'block';
}

document.getElementById('to-feed-scrapper').onclick = () => {
  heading.innerHTML = "Feed Scrapper";
  routes.style.display = 'none';
  back.style.display = 'block';
  feedScrapper.style.display = 'block';

  setHTML(routePath.feedScrapper);
}

document.getElementById('to-cr-scrapper').onclick = () => {
  heading.innerHTML = "Recent CR Scrapper";
  routes.style.display = 'none';
  back.style.display = 'block';
  crScrapper.style.display = 'block';

  setHTML(routePath.crScrapper);
}

document.getElementById('to-home').onclick = () => {
  heading.innerHTML = "Helper Extension";
  routes.style.display = 'block';
  back.style.display = 'none';
  scrapper.style.display = 'none';
  copier.style.display = 'none';
  feedScrapper.style.display = 'none';
  crScrapper.style.display = 'none'
}

// get most recent profile url storage
chrome.storage.local.get(['profileURL'], ({ profileURL }) => {
  document.getElementById('profileURL').value = profileURL || '';
});

document.getElementById('profileURL').oninput = (e) => {
  document.getElementsByClassName('error')[0].style.opacity = e.target.value ? 0 : 1;
}

// send request
const sendRequest = (type, data = {}) => {
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    chrome.tabs.sendMessage(tabs[0].id, { type, ...data });
  });
}

// receive updates
chrome.runtime.onMessage.addListener(
  function(request, _sender, _sendResponse) {
    if (request.type === 'UPDATE_STATUS') {
      setHTML(request.route);
    } else if (request.type === 'ERROR') {
      setError(request.errorMessage);
    }
  }
);

const handleScrape = (salesNav) => {
  let profileURL = document.getElementById('profileURL').value;
  document.getElementsByClassName('error')[0].style.opacity = profileURL ? 0 : 1;

  if (profileURL) {
    profileURL = decodeURIComponent(profileURL);
    document.getElementById('profileURL').value = profileURL;
    chrome.storage.local.set({ profileURL });
    sendRequest(salesNav ? 'SCRAPE_INBOX_SN' : 'SCRAPE_INBOX', { profileURL });
  }
}

const handlePaste = () => {
  sendRequest('PASTE_INBOX');
}

const handleCopyCookie = () => {
  chrome.runtime.sendMessage({ type: 'COPY_COOKIE' });
}

const handlePasteCookie = () => {
  sendRequest('PASTE_COOKIE');
}

const handleScrapeFeed = () => {
  sendRequest('SCRAPE_FEED');
}

const handlePasteFeed = () => {
  sendRequest('PASTE_FEED');
}

const handleScrapeCR = () => {
  sendRequest('SCRAPE_CR');
}

const handlePasteCR = () => {
  sendRequest('PASTE_CR');
}

document.addEventListener("DOMContentLoaded", function () {
  document.getElementById('btn-scrape').addEventListener('click', () => handleScrape(false));
  document.getElementById('btn-scrape-sn').addEventListener('click', () => handleScrape(true));
  document.getElementById('btn-paste').addEventListener('click', handlePaste);
  document.getElementById('btn-copy-cookie').addEventListener('click', handleCopyCookie);
  document.getElementById('btn-paste-cookie').addEventListener('click', handlePasteCookie);
  document.getElementById('btn-scrape-feed').addEventListener('click', handleScrapeFeed);
  document.getElementById('btn-paste-feed').addEventListener('click', handlePasteFeed);
  document.getElementById('btn-scrape-cr').addEventListener('click', handleScrapeCR);
  document.getElementById('btn-paste-cr').addEventListener('click', handlePasteCR);
});
