chrome.runtime.onMessage.addListener(({ type }, _sender, _response) => {
  if (type === 'COPY_COOKIE') {
    chrome.cookies.getAll({ "url": 'https://www.linkedin.com' }, (cookies) => {
      const sessionCookie = cookies.find(({ name }) => name === 'li_at');

      if (sessionCookie) {
        chrome.storage.local.set({ sessionCookie: sessionCookie.value }, () => {
          alert('Cookies copied successfully!');
        });
      } else {
        alert('Could not find session cookie!');
      }
    });

    return true;
  }

  return false;
});