export const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
export const relativeDays = ['Today', 'Yesterday'];
export const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
export const pageLoadWait = 7000;
export const inboxScrapeLimit = 10;
export const postScrapeLimit = 10;
export const crScrapeLimit = 50;
export const status = {
  SCRAPPING: 'Scrapping',
  COMPLETED: 'Completed',
};
export const routes = {
  inboxScrapper: 'INBOX_SCRAPPER',
  feedScrapper: 'FEED_SCRAPPER',
  crScrapper: 'CR_SCRAPPER',
};
