# helper-extension

LinkedIn inbox scrapper

## Development 

This extension was bootstrapped with [Extension CLI](https://oss.mobilefirst.me/extension-cli/)!

### Available Commands

| Commands | Description |
| --- | --- |
| `npm run start` | build extension, watch file changes |
| `npm run build` | generate release version |
| `npm run docs` | generate source code docs |
| `npm run clean` | remove temporary files |
| `npm run test` | run unit tests |
| `npm run sync` | update config files |

### Learn more

Read this guide if you are new to extension development:

[https://developer.chrome.com/extensions/getstarted](https://developer.chrome.com/extensions/getstarted)